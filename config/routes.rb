Rails.application.routes.draw do
  root 'static_pages#home'
  get 'signup' => 'farmers#new' # change to follow \signup path for new farmer
  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'
  resources :farmers
  resources :activities, only: [:create, :destroy, :edit, :update]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
