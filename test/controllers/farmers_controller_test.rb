require 'test_helper'

class FarmersControllerTest < ActionDispatch::IntegrationTest
  def setup
		@farmer = farmers(:michael)
    @other_farmer = farmers(:archer)
	end

  test "should get new" do
    get signup_path
    assert_response :success
  end

  test "should redirect edit when not logged in" do
    #get edit_farmer_path(id: @farmer)
    get edit_farmer_path(@farmer)
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update when not logged in" do
    patch farmer_url(@farmer), params: { farmer: { name: @farmer.name, email: @farmer.email } }
    # This is the original: patch :update, id: @farmer, farmer: { name: @farmer.name, email: @farmer.email }
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect edit when logged in as wrong farmer" do
    log_in_as(@other_farmer)
    get edit_farmer_path(@farmer)
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect index when not logged in" do
    get farmers_path
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Farmer.count' do
      delete farmer_url(@farmer) #params: { id: @farmer }
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when logged in as a non-admin" do
    log_in_as(@other_farmer)
    assert_no_difference 'Farmer.count' do
      delete farmer_url(@farmer) #params: { id: @farmer }
    end
    assert_redirected_to root_url
  end

  test "should not allow the admin attribute to be edited via the web" do
    log_in_as(@other_farmer)
    assert_not @other_farmer.admin?
    patch farmer_url(@other_farmer), params: { farmer: { password: 'password',
                                            password_confirmation: 'password',
                                            admin: true } }
    assert_not @other_farmer.reload.admin?
  end
end
