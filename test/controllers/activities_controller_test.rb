require 'test_helper'

class ActivitiesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @activity= activities(:orange)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Activity.count' do
      post activities_path, params: { activity: { content: "Lorem ipsum", activity_date: Time.zone.now, active: true } }
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Activity.count' do
      delete activity_path(@activity)
    end
    assert_redirected_to login_url
  end
end
