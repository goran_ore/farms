ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all
  include ApplicationHelper
  # This includes app/helpers/application_helper.rb

  # Add more helper methods to be used by all tests here...

  # Returns true if a test farmer is logged in.
  def is_logged_in?
    !session[:farmer_id].nil?
  end

  # Logs in a test farmer.
  def log_in_as(farmer, options = {})
    password = options[:password] || 'password'
    remember_me = options[:remember_me] || '1'
    if integration_test?
      post login_path, params: { session: { email: farmer.email,
                                            password: password,
                                            remember_me: remember_me } }
    else
      session[:farmer_id] = farmer.id
    end
  end

  private
    # Returns true inside an integration test.
    def integration_test?
      defined?(post_via_redirect)
    end
end
