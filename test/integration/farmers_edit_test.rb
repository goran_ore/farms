require 'test_helper'

class FarmersEditTest < ActionDispatch::IntegrationTest
  def setup
    @farmer = farmers(:michael)
  end

  test "unsuccessful edit" do
    log_in_as (@farmer)
    get edit_farmer_path(@farmer)
    assert_template 'farmers/edit'
    patch farmer_path(@farmer), params: { farmer: { name: "",
                                              email: "foo@invalid",
                                              password: "foo",
                                              password_confirmation: "bar" } }
    assert_template 'farmers/edit'
  end

  test "successful edit with friendly forwarding" do
    get edit_farmer_path(@farmer)
    assert_not_nil session[:forwarding_url]
    log_in_as(@farmer)
    assert_redirected_to edit_farmer_path(@farmer)
    name = "Goran"
    email = "goran.ore@gmail.com"
    patch farmer_path(@farmer), params: { farmer: { name: name,
                                      email: email,
                                      password: "foobar",
                                      password_confirmation: "foobar" } }
    assert_not flash.empty?
    assert_redirected_to @farmer
    assert_nil session[:forwarding_url]
    @farmer.reload
    assert_equal @farmer.name, name
    assert_equal @farmer.email, email
  end
end
