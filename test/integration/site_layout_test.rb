require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  test "layout links" do
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", login_path
    farmer = farmers(:michael)
    log_in_as(farmer)
    get root_path
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", farmers_path
    assert_select "a[href=?]", farmer_path(farmer)
    assert_select "a[href=?]", edit_farmer_path(farmer)
  end
end
