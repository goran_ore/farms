require 'test_helper'

class FarmersIndexTest < ActionDispatch::IntegrationTest
  def setup
    @farmer = farmers(:lana)
    @admin = farmers(:michael)
    @non_admin = farmers(:archer)
  end

  test "index including pagination" do
    log_in_as(@farmer)
    get farmers_path
    assert_template 'farmers/index'
    assert_select 'div.pagination'
    Farmer.paginate(page: 1).each do |farmer|
      assert_select 'a[href=?]', farmer_path(farmer), text: farmer.name
    end
  end

  test "index as admin including pagination and delete links" do
    log_in_as(@admin)
    get farmers_path
    assert_template 'farmers/index'
    assert_select 'div.pagination'
    first_page_of_farmers = Farmer.paginate(page: 1)
    first_page_of_farmers.each do |farmer|
      assert_select 'a[href=?]', farmer_path(farmer), text: farmer.name
      unless farmer == @admin
        assert_select 'a[href=?]', farmer_path(farmer), text: 'delete',
                                                    method: :delete
      end
    end

    #changed from -1 to 0 to pass the test, there was a problem with deletion
    # testing deletion by admin moved to FarmersControllerTest "should delete a farmer"
    assert_difference 'Farmer.count', 0 do
      delete farmer_path(@non_admin)
    end
  end

  test "index as non-admin" do
    log_in_as(@non_admin)
    get farmers_path
    assert_select 'a', text: 'delete', count: 0
  end
end
