require 'test_helper'

class FarmersSignupTest < ActionDispatch::IntegrationTest
  test "invalid signup information" do
    get signup_path
    assert_no_difference 'Farmer.count' do
      post farmers_path, params: { farmer: { name: "",
                                          email: "farmer@invalid",
                                          password: "foo",
                                          password_confirmation: "bar" } }
    end
    assert_template 'farmers/new'
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'
  end

  test "valid signup information" do
    get signup_path
    assert_difference 'Farmer.count', 1 do
      post farmers_path, params: { farmer: { name: "Example Farmer",
                                          email: "farmer@example.com",
                                          password: "password",
                                          password_confirmation: "password" } }
      follow_redirect!
    end
    assert_template 'farmers/show'
    assert_not_nil flash
    assert is_logged_in?
  end
end
