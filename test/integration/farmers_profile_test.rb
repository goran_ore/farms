require 'test_helper'

class FarmersProfileTest < ActionDispatch::IntegrationTest
  include ApplicationHelper

  def setup
    @farmer = farmers(:michael)
  end

  test "profile display" do
    get farmer_path(@farmer)
    assert_template 'farmers/show'
    assert_select 'title', full_title(@farmer.name)
    assert_select 'h1', text: @farmer.name
    assert_select 'h1>img.gravatar'
    assert_match @farmer.activities.count.to_s, response.body
    assert_select 'div.pagination'
    @farmer.activities.paginate(page: 1).each do |activity|
      assert_match activity.content, response.body
    end
  end
end
