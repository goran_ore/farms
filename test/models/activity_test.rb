require 'test_helper'

class ActivityTest < ActiveSupport::TestCase
  def setup
    @farmer = farmers(:michael)
    @activity = @farmer.activities.build(content: "Lorem ipsum", activity_date: Time.zone.now, active: true)
  end

  test "should be valid" do
    assert @activity.valid?
  end

  test "farmer id should be present" do
    @activity.farmer_id = nil
    assert_not @activity.valid?
  end

  test "content should be present " do
    @activity.content = " "
    assert_not @activity.valid?
  end

  test "content should be at most 255 characters" do
    @activity.content = "a" * 256
    assert_not @activity.valid?
  end

  test "order should be most recent first" do
    assert_equal Activity.first, activities(:most_recent)
  end
end
