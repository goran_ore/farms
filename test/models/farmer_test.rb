require 'test_helper'

class FarmerTest < ActiveSupport::TestCase
  def setup
    @farmer = Farmer.new(name: "Example Farmer", email: "farmer@example.com",
                    password: "foobar", password_confirmation: "foobar")
  end

  test "should be valid" do
    assert @farmer.valid?
  end

  test "name should be present" do
    @farmer.name = " "
    assert_not @farmer.valid?
  end

  test "email should be present" do
    @farmer.email = " "
    assert_not @farmer.valid?
  end

  test "name should not be too long" do
    @farmer.name = "a" * 51
    assert_not @farmer.valid?
  end

  test "email should not be too long" do
    @farmer.email = "a" * 244 + "@example.com"
    assert_not @farmer.valid?
  end

  test "email validation should accept valid addresses" do
    valid_addresses = %w[farmer@example.com USER@foo.COM A_US-ER@foo.bar.org
    first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @farmer.email = valid_address
      assert @farmer.valid?, "#{valid_address.inspect} should be valid"
    end
  end

  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[farmer@example,com farmer_at_foo.org farmer.name@example.
    foo@bar_baz.com foo@bar+baz.com foo@bar..com.]
      invalid_addresses.each do |invalid_address|
        @farmer.email = invalid_address
        assert_not @farmer.valid?, "#{invalid_address.inspect} should be invalid"
      end
  end

  test "email addresses should be unique" do
    duplicate_farmer = @farmer.dup
    duplicate_farmer.email = @farmer.email.upcase
    @farmer.save
    assert_not duplicate_farmer.valid?
  end

  test "password should have a minimum length" do
    @farmer.password = @farmer.password_confirmation = "a" * 5
    assert_not @farmer.valid?
  end

  test "email addresses should be saved as lower-case" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    @farmer.email = mixed_case_email
    @farmer.save
    assert_equal mixed_case_email.downcase, @farmer.reload.email
  end

  test "authenticated? should return false for a farmer with nil digest" do
    assert_not @farmer.authenticated?('')
  end

  test "associated activities should be destroyed" do
    @farmer.save
    @farmer.activities.create!(content: "Lorem ipsum", activity_date: Time.zone.now)
    assert_difference 'Activity.count', -1 do
      @farmer.destroy
    end
  end
end
