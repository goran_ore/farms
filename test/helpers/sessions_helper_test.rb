require 'test_helper'
class SessionsHelperTest < ActionView::TestCase
  def setup
    @farmer = farmers(:michael)
    remember(@farmer)
  end

  test "current_farmer returns right farmer when session is nil" do
    assert_equal @farmer, current_farmer
    assert is_logged_in?
  end

  test "current_farmer returns nil when remember digest is wrong" do
    @farmer.update_attribute(:remember_digest, Farmer.digest(Farmer.new_token))
    assert_nil current_farmer
  end
end
