# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Farmer.create!(name: "Livija Ore",
            email: "livija.ore@gmail.com",
            password: "123456",
            password_confirmation: "123456",
            admin: true)

Farmer.create!(name: "Goran Ore",
            email: "goran.ore@gmail.com",
            password: "123456",
            password_confirmation: "123456",
            admin: true)

  99.times do |n|
    name = Faker::Name.name
    email = "example-#{n+1}@railstutorial.org"
    password = "password"
    Farmer.create!(name: name,
                email: email,
                password: password,
                password_confirmation: password)
  end

  farmers = Farmer.order(:created_at).take(6)
  50.times do
    content = Faker::Lorem.sentence(1)
    farmers.each { |farmer| farmer.activities.create!(content: content, activity_date: rand(1.year.ago..Time.now)) }
  end
