# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170221082842) do

  create_table "activities", force: :cascade do |t|
    t.text     "content"
    t.boolean  "active",        default: true
    t.boolean  "public",        default: false
    t.datetime "activity_date"
    t.integer  "farmer_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["farmer_id", "activity_date"], name: "index_activities_on_farmer_id_and_activity_date"
    t.index ["farmer_id", "public"], name: "index_activities_on_farmer_id_and_public"
    t.index ["farmer_id"], name: "index_activities_on_farmer_id"
  end

  create_table "farmers", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.boolean  "admin",           default: false
    t.index ["email"], name: "index_farmers_on_email", unique: true
  end

end
