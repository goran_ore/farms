class AddAdminToFarmers < ActiveRecord::Migration[5.0]
  def change
    add_column :farmers, :admin, :boolean, default: false
  end
end
