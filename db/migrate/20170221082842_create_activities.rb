class CreateActivities < ActiveRecord::Migration[5.0]
  def change
    create_table :activities do |t|
      t.text :content
      t.boolean :active, default: true
      t.boolean :public, default: false
      t.datetime :activity_date
      t.references :farmer, foreign_key: true

      t.timestamps
    end
    add_index :activities, [:farmer_id, :activity_date]
    add_index :activities, [:farmer_id, :public]
  end
end
