class AddIndexToFarmersEmail < ActiveRecord::Migration[5.0]
  def change
    add_index :farmers, :email, unique: true
  end
end
