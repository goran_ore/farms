class ActivitiesController < ApplicationController
  before_action :logged_in_farmer, only: [:edit, :create, :destroy, :update]
  before_action :correct_farmer, only: [:edit, :destroy, :update]

  def create
    @activity = current_farmer.activities.build(activity_params)
    if @activity.save
      flash[:success] = "Activity created!"
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def destroy
    @activity.destroy
    flash[:success] = "Scheduled activity deleted"
    redirect_to request.referrer || root_url
  end

  def edit
    store_location_referrer
    @activity = current_farmer.activities.find_by(id: params[:id])
  end

  def update
    @activity = current_farmer.activities.find_by(id: params[:id])
    if @activity.update_attributes(activity_params)
      flash[:success] = "Activity updated"
      redirect_way_back_or(root_url)
    else
      flash[:danger] = "Activity not updated"
    end
  end

  private

    def activity_params
      params.require(:activity).permit(:content, :activity_date,
                                        :farmer_id, :public, :active)
    end

    def correct_farmer
      @activity = current_farmer.activities.find_by(id: params[:id])
      redirect_to root_url if @activity.nil?
    end
end
