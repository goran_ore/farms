class FarmersController < ApplicationController
  before_action :logged_in_farmer,  only: [:index, :edit, :update, :destroy] # Act "only" on these actions
  before_action :correct_farmer,    only: [:edit, :update, :destroy] #added :destroy to fix error in tests for test_should_redirect_destroy
  before_action :admin_farmer, only: :destroy

  def show
    @farmer = Farmer.find(params[:id])
    @activities = Activity.where("farmer_id = ? OR public = ?", @farmer.id, true).paginate(page: params[:page])
    @private_activities = @farmer.activities.paginate(page: params[:page])
    # this last ones shows only activities for current_farmer, we need public ones as well
  end

  def new
    @farmer = Farmer.new
  end

  def create
    @farmer = Farmer.new(farmer_params)
    if @farmer.save
      log_in @farmer
      flash[:success] = "Welcome to Farmers Activity Sample App!"
      redirect_to @farmer
    else
      render 'new'
    end
  end

  def edit
    @farmer = Farmer.find(params[:id])
  end

  def update
     @farmer = Farmer.find(params[:id])
    if @farmer.update_attributes(farmer_params)
      flash[:success] = "Profile updated"
      redirect_to @farmer
    else
      render 'edit'
    end
  end

  def index
    @farmers = Farmer.paginate(page: params[:page])
  end

  def destroy
    @farmer = Farmer.find(params[:id]).destroy
    if current_farmer?(@farmer)
        redirect_to farmers_path, notice: "You can't destroy yourself."
    else
      flash[:success] = "Farmer deleted"
      redirect_to farmers_path
    end
  end

  private
    def farmer_params
      params.require(:farmer).permit(:name, :email, :password,
                                    :password_confirmation)
    end

  # Confirms the correct farmer.
  def correct_farmer
    @farmer = Farmer.find(params[:id])
    redirect_to(root_url) unless current_farmer?(@farmer)
  end

  # Confirms an admin farmer.
  def admin_farmer
    redirect_to(root_url) unless current_farmer.admin?
  end
end
