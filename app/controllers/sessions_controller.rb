class SessionsController < ApplicationController
  def new
  end

  def create
    farmer = Farmer.find_by(email: params[:session][:email].downcase)
    if farmer && farmer.authenticate(params[:session][:password])
      log_in farmer
      params[:session][:remember_me] == '1' ? remember(farmer) : forget(farmer)
      redirect_back_or farmer
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
