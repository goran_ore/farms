class StaticPagesController < ApplicationController
  def home
    if @activity.nil?
      @activity = current_farmer.activities.build if logged_in?
    end
    if logged_in?
      @feed_items = current_farmer.feed.paginate(page: params[:page])
    end
  end
end
