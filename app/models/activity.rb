class Activity < ApplicationRecord
  belongs_to :farmer

  validates :farmer_id, presence: true
  validates :content, presence: true, length: { maximum: 255 }
  validates :activity_date, presence: true
  default_scope { order(activity_date: :desc) }

end
