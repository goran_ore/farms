module FarmersHelper
  # Returns the Gravatar for the given farmer.
  def gravatar_for(farmer, options = { size: 80 })
    gravatar_id = Digest::MD5::hexdigest(farmer.email.downcase)
    size = options[:size]
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    image_tag(gravatar_url, alt: farmer.name, class: "gravatar")
  end
end
