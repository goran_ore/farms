module SessionsHelper
  # Logs in the given farmer.
  def log_in(farmer)
    session[:farmer_id] = farmer.id
  end

  # Returns the current logged-in farmer (if any).
  def current_farmer
    if (farmer_id = session[:farmer_id])  # This is assgnment to farmer_id variable, if value exists
      @current_farmer ||= Farmer.find_by(id: farmer_id)
    elsif (farmer_id = cookies.signed[:farmer_id]) # Also assigment
      farmer = Farmer.find_by(id: farmer_id)
      if farmer && farmer.authenticated?(cookies[:remember_token])
        log_in farmer
        @current_farmer = farmer
      end
    end
  end

  # Returns true if the farmer is logged in, false otherwise.
  def logged_in?
    !current_farmer.nil?
  end

  # Logs out the current farmer.
  def log_out
    forget(current_farmer)
    session.delete(:farmer_id)
    @current_farmer = nil
  end

  # Remembers a farmer in a persistent session.
  def remember(farmer)
    farmer.remember
    cookies.permanent.signed[:farmer_id] = farmer.id
    cookies.permanent[:remember_token] = farmer.remember_token
  end

  # Forgets a persistent session.
  def forget(farmer)
    farmer.forget
    cookies.delete(:farmer_id)
    cookies.delete(:remember_token)
  end

  # Returns true if the given farmer is the current farmer.
  def current_farmer?(farmer)
    farmer == current_farmer
  end

  # Redirects to stored location (or to the default).
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # Stores the URL trying to be accessed.
  def store_location
    session[:forwarding_url] = request.url if request.get? && !request.fullpath.include?("/activities")
  end

  def store_location_referrer
    session[:back_before] = request.referrer if request.get?
  end

  def redirect_way_back_or(default)
    redirect_to(session[:back_before] || default)
    session.delete(:back_before)
  end
end
